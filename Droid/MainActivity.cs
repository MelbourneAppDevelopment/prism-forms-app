﻿using System;

using Android.App;
using Android.Content.PM;
using Android.OS;
using Prism.Unity;
using Microsoft.Practices.Unity;
using Android.Views;
using PrismFormsApp;
using Android.Widget;
using HockeyApp.Android;
using HockeyApp.Android.Metrics;
using PrismFormsApp.Services;
using PrismFormsApp.Droid.Services;

namespace PrismFormsApp.Droid
{
	[Activity(Label = "PrismFormsApp.Droid", Icon = "@drawable/icon", Theme = "@style/BlackTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation )]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
			TabLayoutResource = PrismFormsApp.Droid.Resource.Layout.Tabbar;
			ToolbarResource = PrismFormsApp.Droid.Resource.Layout.Toolbar;

			base.OnCreate(bundle);

			// Handle Orientation: phone = portrait only, table portraite or landscape
			if (Resources.GetBoolean(PrismFormsApp.Droid.Resource.Boolean.is_phone))
			{
				this.RequestedOrientation = ScreenOrientation.Portrait;
			}

			global::Xamarin.Forms.Forms.Init(this, bundle);
			Xamarin.FormsMaps.Init(this, bundle);

			LoadApplication(new PrismFormsApp.App(new AndroidInitializer()));

			// HockeyApp
			CrashManager.Register(this, Settings.HOCKEYAPP_APPID_DROID);
			MetricsManager.Register(this, Application, Settings.HOCKEYAPP_APPID_DROID);
		}

		// Orientation has changed
		public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
		{
			base.OnConfigurationChanged(newConfig);
		}

		public static void SetActionbarText(Activity activity, string text)
		{
			// Setting custom view enable
			if (activity != null)
			{
				activity.ActionBar.SetHomeButtonEnabled(true);
				activity.ActionBar.SetIcon(Android.Resource.Color.Transparent);
				activity.ActionBar.SetDisplayShowCustomEnabled(true);

				LinearLayout linearLayout = new LinearLayout(activity);
				linearLayout.SetGravity(GravityFlags.CenterVertical);
				LinearLayout.LayoutParams textViewParameters =
					new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.MatchParent);
				textViewParameters.RightMargin = (int)(40 * activity.Resources.DisplayMetrics.Density);
				TextView modelTitle = new TextView(activity);
				modelTitle.Text = text;
				modelTitle.Gravity = GravityFlags.Center;
				modelTitle.TextSize = 20;
				modelTitle.SetTextColor(Android.Graphics.Color.White);
				linearLayout.AddView(modelTitle, textViewParameters);
				ActionBar.LayoutParams actionbarParams =
					new ActionBar.LayoutParams(ActionBar.LayoutParams.MatchParent, ActionBar.LayoutParams.MatchParent);
				activity.ActionBar.SetCustomView(linearLayout, actionbarParams);
			}
		}

		protected override void OnResume()
		{
			base.OnResume();

			//HockeyApp
			Tracking.StartUsage(this);
		}

		protected override void OnPause()
		{
			//HockeyApp
			Tracking.StopUsage(this);

			base.OnPause();
		}

	}

	public class AndroidInitializer : IPlatformInitializer
	{
		public void RegisterTypes(IUnityContainer container)
		{
		}
	}


}
