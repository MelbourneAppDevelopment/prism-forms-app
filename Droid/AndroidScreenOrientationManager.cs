﻿using Android.Content.PM;
using PrismFormsApp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: Dependency(typeof(AndroidScreenOrientationManager))]
namespace PrismFormsApp.Droid
{
	public class AndroidScreenOrientationManager 
	{
		FormsApplicationActivity Activity;
		public AndroidScreenOrientationManager(FormsApplicationActivity activity)
		{
			this.Activity = activity;
		}
		public void SetScreenOrientation(ScreenOrientation orientation)
		{
			ScreenOrientation devOrientation = (ScreenOrientation)orientation;

			Activity.RequestedOrientation = devOrientation;
		}
	}
}


