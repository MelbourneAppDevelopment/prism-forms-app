# Prism Forms App #

This is the repo for the Melbourne Xamarin Meetup presentation of a Prism Forms App template.
It is an iOS and Android generic app showing a list of widgets and details of a widget, demonstrating the following features:

* Prism
* MVVM
* A three tier architecture
* Dependency Injections
* Service Locator
* SQLite
* Object disposal
* HockeyApp
* Batch processing for polling a server
* A message bus for handling dynamic change from a server
* Multi-language
* List Views
	* A row as a ViewModel
	* On Item tapped
	* Pull to refresh
	* Swipe to delete
* Xaml
	* Converters
	* Custom Controls
	* Implicit and custom styles


# License #

The MIT License (MIT)

Copyright (c) 2017 Melbounre App Development

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
