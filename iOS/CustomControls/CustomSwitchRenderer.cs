﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using PrismFormsApp;
using PrismFormsApp.CustomControls;
using PrismFormsApp.iOS;
using System;

[assembly: ExportRenderer(typeof(CustomSwitch), typeof(CustomSwitchRenderer))]
namespace PrismFormsApp.iOS
{
	public class CustomSwitchRenderer : SwitchRenderer
	{
		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			var view = (CustomSwitch)this.Element;
			var nativeButton = this.Control;
			nativeButton.OnTintColor = UIColor.Green;
			nativeButton.BackgroundColor = UIColor.Red;
			nativeButton.Layer.BorderColor = UIColor.Black.CGColor;
			nativeButton.Layer.CornerRadius = 16.0f;
		}
	}
}

