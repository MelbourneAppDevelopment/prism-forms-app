﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using Xamarin.Forms;
using Prism.Unity;
using Microsoft.Practices.Unity;
using Xamarin.Forms.Platform.iOS;
using KeyboardOverlap.Forms.Plugin.iOSUnified;
using HockeyApp.iOS;
using PrismFormsApp;
using PrismFormsApp.Services;
using PrismFormsApp.iOS.Services;
using Microsoft.Practices.ServiceLocation;

namespace PrismFormsApp.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{

			// HockeyApp
			var manager = BITHockeyManager.SharedHockeyManager;
			manager.Configure(Settings.HOCKEYAPP_APPID_IOS);
			manager.StartManager();

			UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes()
			{
				TextColor = UIColor.White
			});

			global::Xamarin.Forms.Forms.Init();
			KeyboardOverlapRenderer.Init();

			// Set the status color
			UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.LightContent, false);
			UIApplication.SharedApplication.SetStatusBarHidden(false, false);

			LoadApplication(new App(new iOSInitializer()));

			// Set the tab bar style
			UITabBar.Appearance.SelectedImageTintColor = Color.FromHex("#fcbd36").ToUIColor();
			UITabBar.Appearance.BackgroundColor = UIColor.Black;
			UITabBar.Appearance.BarTintColor = UIColor.Black;
			UITabBar.Appearance.TintColor = UIColor.White;

 			return base.FinishedLaunching(app, options);
		}
	}

	public class iOSInitializer : IPlatformInitializer
	{
		public void RegisterTypes(IUnityContainer container)
		{
			//DependencyService.Register<IAdapter, Adapter>();

		}
	}
}
