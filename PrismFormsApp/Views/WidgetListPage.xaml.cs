﻿using System;
using System.Collections.Generic;
using PrismFormsApp.ViewModels;
using Xamarin.Forms;

namespace PrismFormsApp.Views
{
	public partial class WidgetListPage : ContentPage
	{
        WidgetListPageViewModel ViewModel;
		public WidgetListPage()
		{
			InitializeComponent();
            ViewModel = (WidgetListPageViewModel)BindingContext;
		}

		public void OnItemTapped(object sender, ItemTappedEventArgs e)
		{
			if (e.Item == null)
				return; //ItemSelected is called on deselection, which results in SelectedItem being set to null

			//ViewModel.OnItemTappedCommand.Execute((WidgetRowViewModel)e.Item);
			//((ListView)sender).SelectedItem = null;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			ViewModel.OnAppearing();
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			ViewModel.OnDisappearing();
		}

	}
}
