﻿using System;
using System.Collections.Generic;
using PrismFormsApp.ViewModels;
using Xamarin.Forms;

namespace PrismFormsApp.Views
{
	public partial class WidgetPage : ContentPage
	{
		WidgetPageViewModel ViewModel;

        public WidgetPage()
		{
			InitializeComponent();
				ViewModel = (WidgetPageViewModel)BindingContext;
			}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			ViewModel.OnAppearing();
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			ViewModel.OnDisappearing();
		}
	}
}
