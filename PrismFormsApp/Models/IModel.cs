﻿using System;
using System.Threading.Tasks;

namespace PrismFormsApp.Models
{
    public interface IModel
    {
		int Id { get; set; }

		DateTime CreatedTimestamp { get; set; }
		int CreatedUserId { get; set; }
		DateTime UpdatedTimestamp { get; set; }
		int UpdatedUserId { get; set; }
        bool Changed { get; set; }
   	}
}
