﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.Unity;
using System.Threading.Tasks;
using PrismFormsApp.DatabaseDtos;
using Xamarin.Forms;

namespace PrismFormsApp.Models
{
    public class Widget : BaseModel, IModel, IDisposable
    {
        // Id and UpdateTimestamps inherited from BaseModel to enforce consistency

        public string Name { get; set; }
        public string Description { get; set; }
        public WidgetType WidgetType { get; set; }
        public string WidgetTypeName 
        {
            get
            { 
                return WidgetType.ToString(); 
            }
        }

		#region Constructors
	
		public Widget() : base()
		{
		}

		#endregion
		#region Business Logic Methods
        // Don't put model specific business logic in ViewModels, put it here
		#endregion
		#region DatabaseDtoMethods
        public async Task<int> InitialiseAsync(int id)
        {
            WidgetDto dto;
            if (id == 0)
                dto = new WidgetDto();
            else 
                dto = await _dataService.GetAsync<WidgetDto>(id);
 
            if (dto == null) return 1;

            FromDto(dto, false);
            return 0;
        }

		public int Initialise(WidgetDto dto)
		{
			if (dto == null) return 1;

			FromDto(dto, false);
			return 0;
		}

		public async Task SaveAsync()
		{
 			var dto = new WidgetDto();
			ToDto(ref dto, false);

            if (dto.Id != 0)
                await _dataService.SaveAsync<WidgetDto>(dto);
            else
            {
				await _dataService.SaveAsync<WidgetDto>(dto);
				MessagingCenter.Send<Widget, int>(this, "AddWidget", dto.Id);
			}
                
		}

        public async Task DeleteAsync()
		{
            var dto = await _dataService.GetAsync<WidgetDto>(Id);
            if (dto != null)
                await _dataService.DeleteAsync(dto);
		}

		/// <summary>
		/// Setup database / dto record from model.
		/// </summary>
		/// <param name="dto">Database or Data transfer Object.</param>
		/// <param name="getChildren">If the Dto record contains non-database objects like link records or foreign key records, set this to <true> to load them.</param>
		private void ToDto(ref WidgetDto dto, bool getChildren = true)
        {
 			dto.Name = Name;
			dto.Description = Description;
			dto.WidgetType = (int)WidgetType;

            base.ToDto<WidgetDto>(ref dto);

			if (getChildren)
			{
				// Load children to dto, if any
			}
		}

        /// <summary>
        /// Update field from the Database / Dto record.
        /// </summary>
        /// <param name="dto">Database or Data transfer Object.</param>
        /// <param name="getChildren">If the Dto record contains non-database objects like link records or foreign key records, set this to <true> to load them.</param>
        public void FromDto(WidgetDto dto, bool getChildren = true)
        {
            Name = dto.Name;
            Description = dto.Description;
            WidgetType = (WidgetType)dto.WidgetType;

	        base.FromDto(dto);
	        if (getChildren)
			{
				// Add consequential childred processing, if any
			}
		}

		#endregion
		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~BaseModel() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
