﻿using System;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using PrismFormsApp.DatabaseDtos;
using SQLite;
using Xamarin.Forms;

namespace PrismFormsApp.Models
{
    public class BaseModel 
	{
		#region Properties
 		public int Id { get; set; }

		public DateTime CreatedTimestamp { get; set; }
		public int CreatedUserId { get; set; }
		public DateTime UpdatedTimestamp { get; set; }
        public int UpdatedUserId { get; set; }

        private bool _changed = false;
		public bool Changed             // For synchronising to server
		{
			get { 
                return _changed; 
            }
            set {
                _changed = value;
                //if (_changed)
                //{
                //    var changeEvent = OnChanged;
                //    if (changeEvent != null)
                //        changeEvent(this, new ModelStateChangedEventArgs("changed"));

                //}
			}
		}
		public event EventHandler<ModelChangedEventArgs> OnChanged;


		#endregion
		#region Constructors
		static protected IDataService _dataService;
        public BaseModel ()
        {
            _dataService = ServiceLocator.Current.GetInstance<IDataService>();

            CreatedTimestamp = UpdatedTimestamp = DateTime.UtcNow;
            CreatedUserId = UpdatedUserId = Settings.UserId;
        }
		#endregion
		#region Methods
        protected virtual void ToDto<T>(ref T dto) where T : class, IDto, new()
		{
            dto.Id = Id;

			dto.CreatedTimestamp = CreatedTimestamp;
			dto.CreatedUserId = CreatedUserId;
			dto.UpdatedTimestamp = DateTime.Now;
			dto.UpdatedUserId = UpdatedUserId;
            		}

        protected void FromDto(BaseDto dto)
		{
			Id = dto.Id;

 			CreatedTimestamp = dto.CreatedTimestamp;
			CreatedUserId = dto.CreatedUserId;
			UpdatedTimestamp = dto.UpdatedTimestamp;
			UpdatedUserId = dto.UpdatedUserId;
		}
		#endregion

    }
}

