﻿using System;
namespace PrismFormsApp.Models
{
	public enum WidgetType

	{
        Aqua = 0,
		Black = 1,
		Blue = 2,
		Fuchsia = 3,
		Green = 4,
		Gray = 5,
		Lime = 6,
		Maroon = 7,
		Navy = 8,
		Olive = 9,
		Purple = 10,
		Red = 11,
		Silver = 12,
		Teal = 13,
		Yellow = 14
    }

}
