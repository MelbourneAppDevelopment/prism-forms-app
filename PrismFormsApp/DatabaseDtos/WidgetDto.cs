﻿using SQLite;

namespace PrismFormsApp.DatabaseDtos
{
    public class WidgetDto : BaseDto, IDto
	{
        // Id and UpdateTimestamps inherited from BaseDto to enforce consistency

		public string Name { get; set; }
		public string Description { get; set; }
		public int WidgetType { get; set; }
	}
}

