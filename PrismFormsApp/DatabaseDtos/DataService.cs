using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Unity;
using PrismFormsApp.Helpers;
using System.Threading.Tasks;
using SQLite;
using System.Diagnostics;
using System;
using Xamarin.Forms;
using PrismFormsApp.Models;

// Reference: https://github.com/praeclarum/sqlite-net

namespace PrismFormsApp.DatabaseDtos
{
    public class DataService : IDataService
    {
        #region Private

        static object locker = new object();
        static SQLiteAsyncConnection _connectionAsync;

        static SQLiteConnection _connection;
        #endregion
        #region Constuctor

        public DataService()
        {
            var dbpath = DependencyService.Get<IFileHelper>().GetLocalFilePath("PrismFormsApp.sqlite");

            _connectionAsync = new SQLiteAsyncConnection(dbpath);
            _connection = new SQLiteConnection(dbpath);

            Task.Run(async () =>
            {
                await CreateTablesAsync();
            });

        }
        #endregion
        #region CreateTables
        private async Task CreateTablesAsync()
        {
#if DEBUG
            // Auto recreate on schema change - valuable during development
            try
            {
                await _connectionAsync.CreateTableAsync<WidgetDto>();
            }
            catch
            {
                await _connectionAsync.DropTableAsync<WidgetDto>();
                await _connectionAsync.CreateTableAsync<WidgetDto>();
            }
            await CreateTestdata();
            return;
#endif
            //TODO Crash on schema change. Add auto database update on new schema version.
            await _connectionAsync.CreateTableAsync<WidgetDto>();
        }
        #endregion
        #region Async Methods

        public async Task<int> SaveAsync<T>(T dto) where T : class, IDto, new()
        {
            if (dto.Id == 0)
            {
                dto.CreatedTimestamp = dto.UpdatedTimestamp = DateTime.UtcNow;
                dto.CreatedUserId = dto.UpdatedUserId = Settings.UserId;
                return await _connectionAsync.InsertAsync(dto);
            }
            else
            {
                dto.UpdatedTimestamp = DateTime.UtcNow;
                dto.UpdatedUserId = Settings.UserId;
                return await _connectionAsync.UpdateAsync(dto);
            }
        }

        public async Task<int> InsertAsync(object dto)
        {
            var result = await _connectionAsync.InsertAsync(dto);
            return result;
        }

        public async Task<int> UpdateAsync(object dto)
        {
            try
            {
                return await _connectionAsync.UpdateAsync(dto);
            }
            catch
            {
                return await _connectionAsync.InsertAsync(dto);
            }
        }

        public async Task<int> DeleteAsync(object dto)
        {
            return await _connectionAsync.DeleteAsync(dto);
        }

        public async Task<T> GetAsync<T>(int id) where T : class, IDto, new()
        {
            return await _connectionAsync.Table<T>().Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<List<T>> GetAllAsync<T>() where T : class, IDto, new()
        {
            return await _connectionAsync.Table<T>().ToListAsync();
        }
        public async Task<int> DeleteAllAsync<T>() where T : class, IDto, new()
        {
            await _connectionAsync.DropTableAsync<T>();
            await _connectionAsync.CreateTableAsync<T>();
            return 0;

        }

        public async Task<int> GetCountAsync<T>() where T : class, IDto, new()
        {
            return await _connectionAsync.Table<T>().CountAsync();
        }

        public async Task<DateTime> MaxUpdatedTimestampAsync<T>() where T : class, IDto, new()
        {
            var dto = await _connectionAsync.Table<T>().OrderByDescending(x => x.UpdatedTimestamp).FirstOrDefaultAsync();
            if (dto == null)
                return DateTime.MinValue;
            else
                return dto.UpdatedTimestamp;
        }



        public async Task<List<T>> QueryAsync<T>(string queryText) where T : class, IDto, new()
        {
            return await _connectionAsync.QueryAsync<T>(queryText);
        }
        public async Task<int> QueryScalarAsync(string queryText)
        {
            return await _connectionAsync.ExecuteScalarAsync<int>(queryText);
        }


        #endregion
        private async Task CreateTestdata()
        {
#if DEBUG
            await Task.Delay(1000);     // Crashes if deleting all data while screen is building 
            await DeleteAllAsync<WidgetDto>();

            Widget model;

            model = new Widget();
            model.Name = "Aqua one";
            model.Description = "This is a aqua widget";
            model.WidgetType = WidgetType.Aqua;
            await model.SaveAsync();

            model = new Widget();
            model.Name = "Black one";
            model.Description = "This is a black widget";
            model.WidgetType = WidgetType.Black;
            await model.SaveAsync();

            model = new Widget();
            model.Name = "Blue one";
            model.Description = "This is a blue widget";
            model.WidgetType = WidgetType.Blue;
            await model.SaveAsync();

            model = new Widget();
            model.Name = "Fuchsia one";
            model.Description = "This is a fuchsia widget";
            model.WidgetType = WidgetType.Fuchsia;
            await model.SaveAsync();

            model = new Widget();
            model.Name = "Green one";
            model.Description = "This is a green widget";
            model.WidgetType = WidgetType.Green;
            await model.SaveAsync();

            model = new Widget();
            model.Name = "Gray one";
            model.Description = "This is a gray widget";
            model.WidgetType = WidgetType.Gray;
            await model.SaveAsync();

            model = new Widget();
            model.Name = "Lime one";
            model.Description = "This is a lime widget";
            model.WidgetType = WidgetType.Lime;
            await model.SaveAsync();

            model = new Widget();
            model.Name = "Maroon one";
            model.Description = "This is a maroon widget";
            model.WidgetType = WidgetType.Maroon;
            await model.SaveAsync();

            model = new Widget();
            model.Name = "Navy one";
            model.Description = "This is a navy widget";
            model.WidgetType = WidgetType.Navy;
            await model.SaveAsync();

            model = new Widget();
            model.Name = "Olive one";
            model.Description = "This is a olive widget";
            model.WidgetType = WidgetType.Olive;
            await model.SaveAsync();

            model = new Widget();
            model.Name = "Purple one";
            model.Description = "This is a purple widget";
            model.WidgetType = WidgetType.Purple;
            await model.SaveAsync();

            model = new Widget();
            model.Name = "Red one";
            model.Description = "This is a red widget";
            model.WidgetType = WidgetType.Red;
            await model.SaveAsync();

            model = new Widget();
            model.Name = "Silver one";
            model.Description = "This is a silver widget";
            model.WidgetType = WidgetType.Silver;
            await model.SaveAsync();

            model = new Widget();
            model.Name = "Teal one";
            model.Description = "This is a teal widget";
            model.WidgetType = WidgetType.Teal;
            await model.SaveAsync();

            model = new Widget();
            model.Name = "Yellow one";
            model.Description = "This is a yellow widget";
            model.WidgetType = WidgetType.Yellow;
            await model.SaveAsync();
 
            #endif
        }
    }
}