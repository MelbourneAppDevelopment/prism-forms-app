using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrismFormsApp.DatabaseDtos
{
    public interface IDataService {

        Task<int> SaveAsync<T>(T dto) where T : class, IDto, new();
        Task<int> InsertAsync(object dto);
        Task<int> UpdateAsync(object dto);
        Task<int> DeleteAsync(object dto);

        Task<T> GetAsync<T>(int id) where T : class, IDto, new();
        Task<List<T>> GetAllAsync<T>() where T : class, IDto, new();
        Task<int> DeleteAllAsync<T>() where T : class, IDto, new();
        Task<int> GetCountAsync<T>() where T : class, IDto, new();

        Task<DateTime> MaxUpdatedTimestampAsync<T>() where T : class, IDto, new();

        Task<List<T>> QueryAsync<T>(string queryText) where T : class, IDto, new();
        Task<int> QueryScalarAsync(string queryText);

    }
}