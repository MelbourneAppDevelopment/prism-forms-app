﻿using System;
namespace PrismFormsApp
{
    public interface IDto
    {
		int Id { get; set; }

		DateTime CreatedTimestamp { get; set; }
		int CreatedUserId { get; set; }
		DateTime UpdatedTimestamp { get; set; }
		int UpdatedUserId { get; set; }

		bool Changed { get; set; }
	}
}
