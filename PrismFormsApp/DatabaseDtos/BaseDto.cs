﻿using System;
using System.Reflection;
using SQLite;

namespace PrismFormsApp.DatabaseDtos
{
	public class BaseDto 
	{
		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }

		public DateTime CreatedTimestamp { get; set; }
		public int CreatedUserId { get; set; }
		public DateTime UpdatedTimestamp { get; set; }
		public int UpdatedUserId { get; set; }

		private bool _changed = false;
		public bool Changed             // For synchronising to server
		{
			get { return _changed; }
			set { _changed = value; }
		}

	}
}

