﻿using System;
using System.Diagnostics;
using PrismFormsApp.Views;
using Microsoft.Practices.Unity;
using Prism.Unity;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PrismFormsApp.Services;
using PrismFormsApp.DatabaseDtos;
using PrismFormsApp.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using PrismFormsApp.Helpers;
using Microsoft.Practices.ServiceLocation;
using Helpers;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace PrismFormsApp
{

	public partial class App : PrismApplication
	{
		//static public int ScreenWidth;
		//static public int ScreenHeight;
		TimerState batchTimer;

		public App(IPlatformInitializer initializer = null) : base(initializer)
		{
		}

		protected override void OnInitialized()
		{
            InitializeComponent();
			var navPage = new NavigationPage(new TabPage())
			{
				BarBackgroundColor = Color.Black,
				BarTextColor = Color.White
			};

 			MainPage = navPage;
            NavigationService.NavigateAsync("TabPage");
		}

		protected override void RegisterTypes()
		{
			ServiceLocator.SetLocatorProvider(() => new UnityServiceLocator(Container));

			// IoC 
			Container.RegisterType<IDataService, DataService>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IWidgetController, WidgetController>(new ContainerControlledLifetimeManager());

			// Navigation 
			Container.RegisterTypeForNavigation<TabPage>();
			Container.RegisterTypeForNavigation<WidgetPage>();
			Container.RegisterTypeForNavigation<AboutPage>();
			Container.RegisterTypeForNavigation<WidgetListPage>();

			// Platform Dependencies
			Container.RegisterType<IDeviceService>();

			// Mulit-language
			var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
			PrismFormsApp.Resources.AppResources.Culture = ci; // set the RESX for resource localization
			DependencyService.Get<ILocalize>().SetLocale(ci); // set the Thread for locale-aware methods
		}

		protected override void OnStart()
		{
			HockeyApp.MetricsManager.TrackEvent("App Starting");
		}

		protected override void OnSleep()
		{
			MessagingCenter.Send(this, "OnSleep");
			HockeyApp.MetricsManager.TrackEvent("App going to sleep");
		}

		protected override void OnResume()
		{
			MessagingCenter.Send(this, "OnResume");
			HockeyApp.MetricsManager.TrackEvent("App resuming");
		}

		#region Batch Processing
		private void StartTimer()
		{
			batchTimer = new TimerState();
			TimerCallback timerDelegate = new TimerCallback(ThreadedBatchProcess);
			Timer timer = new Timer(timerDelegate, batchTimer, 1000, 1000);
			batchTimer.tmr = timer;  // So we have a handle to dispose
		}

		private void StopTimer()
		{
			if (batchTimer != null && batchTimer.tmr != null)
			{
				batchTimer.tmr.Cancel();
				batchTimer.tmr.Dispose();
				batchTimer = null;
			}
		}

		private void ThreadedBatchProcess(Object state)
		{
			Debug.WriteLine("Batch process timer ran");
			// Do stuff
		}
        #endregion
 
	}
}
