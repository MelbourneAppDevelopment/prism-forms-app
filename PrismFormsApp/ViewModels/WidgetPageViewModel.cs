﻿using System;
using Xamarin.Forms;
using Prism.Commands;
using Prism.Mvvm;
using System.Collections.Generic;
using PrismFormsApp.Models;
using System.Diagnostics;
using Prism.Navigation;
using PrismFormsApp.Resources;
using System.ComponentModel;
using Helpers;
using Microsoft.Practices.Unity;
using PrismFormsApp.DatabaseDtos;
using System.Threading.Tasks;

namespace PrismFormsApp.ViewModels
{
    public class WidgetPageViewModel : BindableBase, INavigatedAware, INotifyPropertyChanged, IDisposable
	{
		#region Constructors
		private readonly INavigationService _navigationService;
		private readonly IDataService _dataService;

		public WidgetPageViewModel(
			IDataService dataService,
			INavigationService navigationService
		)
		{
			_dataService = dataService;
			_navigationService = navigationService;

		}
        #endregion
        #region private Proterties

        #endregion
        #region UIProperties
        private Widget _widget;
        public Widget Widget 
        { 
            get
            {
                return _widget;
            }
            set
            {
                _widget = value;
                FromWidget(_widget);
            }
        }

		int _id;
		public int Id
		{
			get
			{
				return _id;
			}
			set
			{
				SetProperty(ref _id, value);
			}
		}

		string _name;
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				SetProperty(ref _name, value);
			}
		}

		string _description;
		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				SetProperty(ref _description, value);
			}
		}

		string _widgetType;
		public string WidgetType
		{
			get
			{
				return _widgetType;
			}
			set
			{
				SetProperty(ref _widgetType, value);
			}
		}

		bool _changed;
		public bool Changed
		{
			get
			{
				return _changed;
			}
			set
			{
				SetProperty(ref _changed, value);
			}
		}

		string _errorMessage = "";
		public string ErrorMessage
		{
			get
			{
				return _errorMessage;
			}
			set { SetProperty(ref _errorMessage, value); }
		}
		#endregion
		#region Commands
		private DelegateCommand _helpCommand;
		public DelegateCommand HelpCommand => _helpCommand != null ? _helpCommand : (_helpCommand = new DelegateCommand(DoHelpCommand));
		private void DoHelpCommand()
		{

		}

        #endregion
        #region public methods
 			public void OnAppearing()
		{
		}

		public void OnDisappearing()
		{
		}
		#endregion
		#region private Methods
		void OnWidgetChanged(object sender, ModelChangedEventArgs e)
		{
			var widget = (Widget)sender;
            FromWidget(widget);
		}
        void FromWidget(Widget widget)
        {
			Id = widget.Id;
			Name = widget.Name;
			Description = widget.Description;
			WidgetType = widget.WidgetType.ToString();
		}
		#endregion

		#region INavigateAware
		public void OnNavigatedFrom(NavigationParameters parameters)
		{
		}

		public void OnNavigatedTo(NavigationParameters parameters)
		{
			if (parameters.ContainsKey("WidgetId"))
			{
                var widgetId = (int)parameters["WidgetId"];
				Widget = new Widget();
				if (widgetId != 0)
				{
                    Task.Run(async () =>
                    {
                        var result = await Widget.InitialiseAsync(widgetId);
                        FromWidget(Widget);
                    });
    			}
			}
			else
			{
				Widget = new Widget();
                //TODO updating changes if REST interface installed
				//Widget.OnChanged -= OnWidgetChanged;
				//Widget.OnChanged += OnWidgetChanged;
			}
		}

		#endregion
		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                //Widget.OnChanged -= OnWidgetChanged;
 
                disposedValue = true;
            }
        }

         ~WidgetPageViewModel() {
           // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
           Dispose(false);
         }

         public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
             GC.SuppressFinalize(this);
        }
        #endregion

    }
}
