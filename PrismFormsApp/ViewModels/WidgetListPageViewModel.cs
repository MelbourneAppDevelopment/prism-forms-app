﻿using System;
using Xamarin.Forms;
using Prism.Commands;
using Prism.Mvvm;
using System.Collections.Generic;
using PrismFormsApp.Models;
using System.Diagnostics;
using Prism.Navigation;
using PrismFormsApp.Resources;
using System.ComponentModel;
using Helpers;
using Microsoft.Practices.Unity;
using Microsoft.Practices.ServiceLocation;
using Prism.Services;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Linq;

namespace PrismFormsApp.ViewModels
{
    public class WidgetListPageViewModel : BindableBase, INavigatedAware, INotifyPropertyChanged
	{
		#region Constructors
		private readonly INavigationService _navigationService;
		private readonly IWidgetController _widgetController;
		private readonly IPageDialogService _pageDialogService;

		public WidgetListPageViewModel(
			IWidgetController widgetController,
			INavigationService navigationService,
			IPageDialogService pageDialogService
		)
		{
			_widgetController = widgetController;
			_navigationService = navigationService;
			_pageDialogService = pageDialogService;

			// Subscribe to message to delete a widget from the ObservableCollection
			MessagingCenter.Subscribe<Widget, int>(this, "DeleteWidget", (sender, widgetId) =>
		   {
			   Device.BeginInvokeOnMainThread( () =>
				 {
                    Widgets.Remove(sender);
				 });

		   });
			MessagingCenter.Subscribe<Widget, int>(this, "AddWidget", (sender, widgetId) =>
		   {
			   Device.BeginInvokeOnMainThread( () =>
				 {
                     Widgets.Add(sender);
 				 });

		   });
		}
		#endregion
		#region UIProperties
		bool _isRefreshing;
		public bool IsRefreshing
		{
			get
			{
				return _isRefreshing;
			}
			set
			{
				SetProperty(ref _isRefreshing, value);
			}
		}

        ObservableCollection<Widget> _widgets = new ObservableCollection<Widget>();
        public ObservableCollection<Widget> Widgets
        {
            get
            {
                return _widgets;
            }
            set { SetProperty(ref _widgets, value); }
        }

        string _errorMessage = "";
		public string ErrorMessage
		{
			get
			{
				return _errorMessage;
			}
			set { SetProperty(ref _errorMessage, value); }
		}
		#endregion
		#region ICommands
		private DelegateCommand _refreshListCommand;
		public DelegateCommand RefreshListCommand => _refreshListCommand != null ? _refreshListCommand : (_refreshListCommand = new DelegateCommand(DoRefreshListCommand));
		private void DoRefreshListCommand()
		{
			IsRefreshing = true;
            Task.Delay(4000); // simulate doing something
			IsRefreshing = false;
		}

        private DelegateCommand<Widget> _onItemTappedCommand;
		public DelegateCommand<Widget> OnItemTappedCommand => _onItemTappedCommand != null ? _onItemTappedCommand : (_onItemTappedCommand = new DelegateCommand<Widget>(async (item) => await DoOnItemTappedCommand(item)));
		private async Task DoOnItemTappedCommand(Widget item)
		{
			var param = new NavigationParameters();
            param.Add("WidgetId", item.Id);

			await _navigationService.NavigateAsync("WidgetPage", param, false, true);
		}

        private DelegateCommand<Widget> _sellCommand;
        public DelegateCommand<Widget> SellCommand => _sellCommand != null ? _sellCommand : (_sellCommand = new DelegateCommand<Widget>(async (item) => await DoSellCommand(item)));
        private async Task DoSellCommand(Widget item)
        {
            var message = String.Format("You sold a {0} widget.", item.WidgetType);
            await _pageDialogService.DisplayAlertAsync("Excellent!", message, "Ok");
        }

        private DelegateCommand<Widget> _deleteCommand;
        public DelegateCommand<Widget> DeleteCommand => _deleteCommand != null ? _deleteCommand : (_deleteCommand = new DelegateCommand<Widget>(async (item) => await DoDeleteCommand(item)));
        private async Task DoDeleteCommand(Widget item)
        {
            var message = String.Format("Are you sure you want to delete the {0} one?", item.WidgetType);
            var response = await _pageDialogService.DisplayAlertAsync("Delete", message, "Ok", "Cancel");
            if (response)
            {
                Widgets.Remove(item);
                await item.DeleteAsync();
            }
        }

        private DelegateCommand _helpCommand;
		public DelegateCommand HelpCommand => _helpCommand != null ? _helpCommand : (_helpCommand = new DelegateCommand(DoHelpCommand));
		private void DoHelpCommand()
		{
 			_pageDialogService.DisplayAlertAsync(
                AppResources.Help,
                AppResources.WidgetListHelpText, 
                AppResources.GotIt
            );
		}

		#endregion
		#region public methods
		public void OnAppearing()
		{
 			Task.Run(async () =>
			{
				await LoadWidgetsAsync();
			});

		}

		public void OnDisappearing()
		{
		}
        #endregion
		#region private Methods
		private async Task LoadWidgetsAsync()
        {
            var widgets = await _widgetController.GetAllAsync();
            Widgets.Clear();

            if (widgets != null)
                foreach (var widget in widgets)
                {
                    Widgets.Add(widget);
                }
		}

		#endregion
		#region INavigateAware
		public void OnNavigatedFrom(NavigationParameters parameters)
		{
		}

		public void OnNavigatedTo(NavigationParameters parameters)
		{
		}
        #endregion
	}
}
