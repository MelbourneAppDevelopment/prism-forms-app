﻿using System;
using Xamarin.Forms;

namespace PrismFormsApp.Converters
{
	public class SwitchColorConverter : IValueConverter
	{
        //-- Setting the color of a switch based on the switch being 'true' or 'false' --/

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if ((bool)value)
				return Color.Green;
			else
				return Color.Red;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if ((bool)value)
				return Color.Green;
			else
				return Color.Red;
		}
	}
}
