﻿using System;

public class ModelChangedEventArgs : EventArgs
{
    //--- If a Widget change is experience on the server then this needs to be triggered and actioned --//
	public string State { get; set; }
	public ModelChangedEventArgs(string state)
	{
		State = state;
	}
}
