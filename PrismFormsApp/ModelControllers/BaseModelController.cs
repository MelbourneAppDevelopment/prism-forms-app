﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using PrismFormsApp.DatabaseDtos;
using PrismFormsApp.Models;

namespace PrismFormsApp
{
    public class BaseModelController<T> where T : class, IModel, new()
    {
 		static protected IDataService _dataService;
		public BaseModelController()
		{
			_dataService = ServiceLocator.Current.GetInstance<IDataService>();
		}

	}
}
