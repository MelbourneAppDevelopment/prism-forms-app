﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using PrismFormsApp.DatabaseDtos;
using PrismFormsApp.Models;

namespace PrismFormsApp
{
    public class WidgetController : BaseModelController<Widget>, IWidgetController
    {
        public WidgetController() : base()
        {
        }

		public async Task<Widget> GetAsync(int id)
        {
            var widget = new Widget();
            if (id == 0)
                return widget;
            await widget.InitialiseAsync(id);
            return widget;
        }

        public async Task<ObservableCollection<Widget>> GetAllAsync()
        {
            
			var result = new ObservableCollection<Widget>();
            var dtos = await _dataService.GetAllAsync<WidgetDto>();
            foreach (var dto in dtos){
                var widget = new Widget();
                widget.Initialise(dto);
                result.Add(widget);
            }

            // Other logic like sort the array

			return result;
		}
	}
}
