﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using PrismFormsApp.Models;

namespace PrismFormsApp
{
    public interface IWidgetController
    {
		Task<Widget> GetAsync(int id);
		Task<ObservableCollection<Widget>> GetAllAsync();

        // Sync with server methods

    }
}
