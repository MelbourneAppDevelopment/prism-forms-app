﻿using System;
namespace PrismFormsApp.Helpers
{
	public interface IFileHelper
	{
		string GetLocalFilePath(string filename);
	}
}
